import React from 'react';
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const url_record = "https://skywalker111.pythonanywhere.com/api/dishes/";

class NewDish extends React.Component {
    state = {
        create_success: undefined,
    }

    get_authors = async () => {
        try {
            const response = await fetch(url_record, {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + this.props.token
                },
            })
        } catch (e) {
            this.setState({
                error: "Error!"
            })
        }
    }

    

    regRecord = async () => {
        let title = this.title.value;
        let chef = this.chef.value;
        let price = this.price.value;
        try {
            const response = await fetch(url_record, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Token ' + this.props.token
                },
                body: JSON.stringify({
                    'title': title,
                    'chef' :chef,
                    'price': price
                })
            })
            let temp = await response.json();
            if (response.ok) {
                this.setState({create_success: true})
            } else {
                console.log(temp);
                this.setState({create_success: false})
            }

        } catch (e) {
            this.setState({
                error: "Error!"
            })
        }
    }

    componentDidMount = async () => {
        await this.get_authors();
    }

    render() {
        let {create_success} = this.state;

        return (
            <div className="leftmodal">
                <div><h1 className="h1">Новое блюдо</h1></div>
                <Form>
                    <Form.Group className="mb-3">
                        <Form.Label>Название</Form.Label>
                        <Form.Control type="text" name="title" placeholder="Enter price"
                                      ref={ref => this.title = ref}/>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Повар</Form.Label>
                        <Form.Control type="number" name="chef" placeholder="Enter chef"
                                      ref={ref => this.price = ref}/>
                    </Form.Group>
                    
                    <Form.Group className="mb-3">
                        <Form.Label>Цена</Form.Label>
                        <Form.Control type="number" name="price" placeholder="Enter price"
                                      ref={ref => this.price = ref}/>
                    </Form.Group>

                    <Button onClick={this.regRecord} variant="primary">
                        Создать
                    </Button>

                    {create_success ? <div className="create_success"><p>ДобавленоЫ!</p></div> :
                        <div></div>
                    }
                </Form>
            </div>
        )
    }
}

export default NewDish;