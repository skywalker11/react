import React from 'react';
import Button from 'react-bootstrap/Button';
import '../App.css';

const buttonArray = [
    {
        text: 'Список блюд',
        title: 'dish list'
    },
    {
        text: 'Новое блюдо',
        title: 'new dish'
    }
]

class Navigator extends React.Component{
    render(){
        const {changeWindow} = this.props;
        return (
            <div>
                {buttonArray.map((button) => (
                    <div>
                        <Button className="menu_buttons" variant="primary" onClick={changeWindow.bind(this, button.title)}>{button.text}</Button >
                    </div>
                ))}
            </div>
        )
    }
}

export default Navigator;