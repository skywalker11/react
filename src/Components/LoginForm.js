import React, {Component} from 'react';
import Form from 'react-bootstrap/Form';
import {Button, Container} from "react-bootstrap";
import Header from './Header'
import Navigator from './Navigation'
import Dishes from '../Tables/Dishes';
import NewDish from '../Tables/NewDish';
import '../App.css';


class LoginForm extends Component {

    constructor(props) {
        super(props);

        this.log = null;
        this.pas = null;

        this.state = {
            visibility: true,
        };

        this.state_error = {
            vis: false
        }

        this.Login = this.Login.bind(this);
        this.handleClick = this.handleClick.bind(this);

    }

    changeWindow = (new_window) => {
        this.setState({activeWindow: new_window})
    }

    handleClick() {
        this.setState(state => ({
            visibility: !state.visibility
        }));
        this.setState({vis: false})
    }

    Login = async () => {
        let username = this.log.value;
        let password = this.pas.value;

        try {
            const response = await fetch('http://skywalker111.pythonanywhere.com/api-token-auth/', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    "username": username,
                    "password": password
                })
            });

            let s_token = await response.json();
            if (response.ok) {
                this.setState(state => ({
                    visibility: !state.visibility
                }));
                this.props.setTokenn(s_token.auth_token);
            } else {
                this.setState({vis: true});
            }
        } catch
            (e) {
            console.log("ERROR!")
        }
    }

    render() {
        if (this.state.visibility) {
            return (
                <div style={{textAlign: "center"}}>
            <Container class="box">
                    <h1>LOG IN</h1>
                <div><Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Control type="text" name="username" placeholder="USERNAME:"
                                          ref={ref => this.log = ref}/></Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Control type="password" name="password" placeholder="PASSWORD:"
                                          ref={ref => this.pas = ref}/></Form.Group>

                        <Button className ='enter-button' onClick={this.Login} variant="primary">ENTER</Button>

                        {this.state.vis ? <div className="error">INCORRECT DATA!<br></br> TRY AGAIN</div> : <div></div>}
                    </Form>
                </div>
            </Container></div>
            );
            
        }else {
        return (
            <div><Header handleClick={this.handleClick}/>
                <Container>
                    <div><div className='MainScreen'>
                    <Navigator changeWindow={this.changeWindow}/>
                        {(() => {
                           switch (this.state.activeWindow) {
                                case 'dish':
                                    return (
                                        <Dishes token={this.props.token}/>
                                    )
                                case 'new dish':
                                    return (
                                        <NewDish token={this.props.token}/>
                                    )
                                default:
                                    return (
                                        <Dishes token={this.props.token}/>
                                    )
                                }

                            })()}
                            </div>
                    </div>
                    </Container>
                </div>
            );
        }
    }
}
    

export default LoginForm;