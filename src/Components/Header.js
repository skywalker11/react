import React, {Component} from 'react';
import {Button, Container, Nav, Navbar} from "react-bootstrap";
import '../App.css'

class Header extends Component {

    render() {
        return (
         <div><Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand>Мясников Александр</Navbar.Brand>
                        <Nav><Button  className = "exit_button"  onClick={this.props.handleClick}>EXIT</Button></Nav>
                </Container>
              </Navbar>
          </div>
        );
    }
}

export default Header;
